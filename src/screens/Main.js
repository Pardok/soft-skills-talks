import React, { useState, useEffect, useRef, useCallback } from 'react';
import { Box, Card, Heading, Text, Button } from 'rebass'

import textToSpeech from '../tts'

const dialog = require('../dialog.json')

const listenSpeech = () => {}

const Dialog = ({ item, nextID, back }) => {
  const isUserInput = (item.type === 'UserInput')
  const [isListening, setListening] = useState(false)
  
  const text = isUserInput ? item.expectedText : item.speakText
  //console.log({id: item.id, next: nextID, back: back})

  useEffect(
    () => {
      if (isUserInput) {
        listenSpeech(text)
      } else {
        isListening || setListening(true)
        textToSpeech(text).then(()=>setListening(false))
      }
    },
    [item],
  );
  
  return (
    <Box>
      <Box className="upper-half">
        <Card sx={{alignSelf: 'flex-end'}} m={2}>
          <Heading m={2}>{(item.type === 'UserInput') ? 'Произнесите' : 'Слушайте'}</Heading>
          <Text m={2}>{text}</Text>
        </Card>
      </Box>
      <Box className="lower-half">
        <Button m={2} onClick={!isListening ? nextID : undefined} bg={isListening ? 'gray' : undefined}>Дальше</Button>
        {isUserInput && back !== undefined ? <Button m={2} onClick={back} variant='secondary'>Повторить</Button> : null}
      </Box>
    </Box>
  )
}

const Results = ({ retries, timeSpent }) => {
  console.log(timeSpent)
  return (
    <Card>
      <Heading m={2}>Результат</Heading>
      <Text m={2}>Повторов: {retries}</Text>
      <Text m={2}>Время: {Math.round(timeSpent/10)/100} sec</Text>
    </Card>
  )
}

const MainScreen = () => {
  const [ currentID, setID ] = useState(0)

  const userInputs = useRef(new Set())
  const retries = useRef(0)
  const startTime = useRef(Date.now())

  const showResults = (currentID === dialog.length)
  
  if (!showResults && !userInputs.current.has(currentID) && dialog[currentID].type === 'UserInput' ) {
    userInputs.current.add(currentID)
  }

  const nextID = useCallback (() => {
    setID(currentID + 1)
  }, [currentID])

  const backToLastUserInput = () => {
    retries.current++
    console.log('userInputs', userInputs, 'currentID', currentID)
    for (let i = currentID - 1; i >= 0; i--) {
      if (userInputs.current.has(i)) {
        console.log('Back to ', i)
        setID(i)
        break
      }
    }
  }

  return (
    <Box className="App-centerer">
      {!showResults
        ? <Dialog item={dialog[currentID]} nextID={nextID} back={ userInputs.current.size > 1 && [...userInputs.current][0] !== currentID ? backToLastUserInput : undefined}/>
        : <Results retries={retries.current} timeSpent={Date.now() - startTime.current}/>}
      <audio id='audio'/>
    </Box>
  )
}

export default MainScreen