import React from 'react';
import './App.css';
import { ThemeProvider } from 'emotion-theming'
import { Box } from 'rebass'

import theme from './theme.js'

import MainScreen from './screens/Main'

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Box className="App">
        <MainScreen />
      </Box>
    </ThemeProvider>
  );
}

export default App;
