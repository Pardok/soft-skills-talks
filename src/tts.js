const TTS_SERVER_URL = window.location.href.split(':').slice(0, -1).join(':') + ':3333/tts'
const TTS_ENABLED = false
const NO_TTS_DELAY = 1000

const textToSpeech = (text) => {

    if (!TTS_ENABLED) {
        return new Promise((resolve)=>{setTimeout(resolve, NO_TTS_DELAY)})
    }
  
    console.log(JSON.stringify({text: text}))
    
    const params = new URLSearchParams();
    params.append('text', text);
  
    const promise = new Promise((resolve, reject) => {
        fetch(TTS_SERVER_URL, {
            method: 'POST',
            body: params,
            headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then( res =>
            res.body.getReader().read()
        ).then( data =>
            playFetchedAudio(data)
        ).then( endPromise =>
            resolve(endPromise)
        ).catch( e => {
            console.warn(e)
            reject(false)
        })
    })
    return promise
  }
  
const playFetchedAudio = (response) => {
    const blob = new Blob([response.value], { type: 'audio/mp3' });
    const url = window.URL.createObjectURL(blob)
    window.audio = new Audio(url);
    window.audio.play();
    return new Promise((resolve) =>{
        window.audio.onended = () => {
            resolve(true)
        }
    })
}

export default textToSpeech