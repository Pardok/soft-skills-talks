import theme from '@rebass/preset'

const defaultTheme = {
    ...theme,
    buttons: {
        ...theme.buttons,
        disabled: {
            color: "muted",
            bg: "muted",
            variant: "buttons.primary",
        }
    }
}

export default defaultTheme